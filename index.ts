import Service from './lib/Service/'
import Server from './lib/Server';
import mergeFile from './includes/mergeFiles';
import * as _ from 'lodash';

const appService = new Service()

mergeFile('modules', function(filePath) {
    if(filePath.indexOf('services')!== -1) {
        const { services } = require(`./${filePath.replace('.ts', '')}`);
        _.forIn(
            services,(value, key) => (
                appService.addService({name: key, service: new value()})
            )
        )
    }
});


const port: number = 3000;
const app = Server.bootstrap().app;

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}/`);
});

console.log('user', Server.Service('User').renderUser('asda'));
console.log('Baba', Server.Service('Client').renderClient('asdasdas'));
console.log('ss', Server.Service('Baba').renderClient('asdasasdasddas'));
console.log('math', Server.Service('myMath').sum(1, 4));
console.log('sss', Server.Service('dupa').render(3, 4));
console.log('customer', Server.Service('Customer').render("sss"));

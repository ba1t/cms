import * as _ from 'lodash';

export class Service {

    private static _instance: Service;
    private _services: any[] = [];

    constructor() {
        if(Service._instance){
            return Service._instance;
        }
        Service._instance = this
    }

    public init(services: object[]): void {
        this._services = services;
    }

    public addService(service: any): void {
        this._services = [...this._services, {name: service.name, service: service.service}];
    }

    public getService(name: string): object {
        return _.find(this._services, (service) =>(
            service.name == name  
        )).service
    } 

}

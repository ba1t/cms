import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as logger from "morgan";
import * as _ from 'lodash';
import Service from './Service'

export default class Server {

    private static _instance: Server;
    
    public app: express.Application;
    
    public router: express.Router;
    
    
    constructor() {
        if(Server._instance){
            return Server._instance;
        }
        Server._instance = this
        new Service();
        this.app = express();
        this.router = express.Router();
        this.config();
        this.api();
        this.app.use(this.router);
        
    }

    public static bootstrap(): Server {
        return new Server();
    }

    // public static Database(): Database {
    //     return new Database();
    // }

    public static Service(name: string): any {
        return new Service().getService(name);
    }




    public api() {
        this.router.get('/api', (req: express.Request, res: express.Response) => {
            res.json({api: "1.0"});
        });
    }

    public config() {
        this.app.use(express.static('sites'));
        this.app.use(logger("dev"));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({
          extended: true
        }));
        this.app.use(cookieParser("SECRET_GOES_HERE"));
      
        this.app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
            err.status = 404;
            next(err);
        });
    }

}